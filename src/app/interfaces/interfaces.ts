export interface HousePost {
    title:string,
    url:string,
    crime:number,
    price:string,
    address:string,
    details:string,
    type:string,
    src:string
}