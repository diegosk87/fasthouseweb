import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HousePost } from '../interfaces/interfaces';

const apiUrl = "http://10.174.67.37:3000";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http:HttpClient) { }

  public getData() {
    return this.http.get<any>(`${apiUrl}/api/hotels`);
  }
}
