import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { HousePost } from 'src/app/interfaces/interfaces';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  public houses:HousePost[];

  constructor(private dataService:DataService) {}

  ngOnInit() {
      this.dataService.getData()
        .subscribe(response => {
          this.houses = response.data;
        });
  }
}
